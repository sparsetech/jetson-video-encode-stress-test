# Usage

Just run the stress script:

```bash
./stress.sh loop
```

It will create a raw test video of size ~100 MB with random data. Script:

1. will spawn 8 1080p High profile video encoder processes
2. wait for all processes to finish
3. If no error is encountered, go to step 1.
4. If at least one of the encoders fail, print error message and exit.

Outputs of each iteration will be collected in a dedicated folder for 
post-mortem (manual) analysis.
