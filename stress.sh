#!/bin/bash

#start with a clean dir structure
rm -rf logs/
#create our test video
dd if=/dev/urandom of=test.raw bs=3110400 count=30

iter=1
while [[ "$1" == "loop" ]]
do
	./test.sh tv1 > tv1.log 2>&1 &
	./test.sh tv2 > tv2.log 2>&1 &
	./test.sh tv3 > tv3.log 2>&1 &
	./test.sh tv4 > tv4.log 2>&1 &
	./test.sh tv5 > tv5.log 2>&1 &
	./test.sh tv6 > tv6.log 2>&1 &
	./test.sh tv7 > tv7.log 2>&1 &
	./test.sh tv8 > tv8.log 2>&1 &

	cnt=$(ps -A | grep -i test.sh | wc -l)

	while [[ "$cnt" != "0" ]]
	do
		echo waiting
		sleep 1
		cnt=$(ps -A | grep -i test.sh | wc -l)
	done

	mkdir -p logs/$iter
	mv tv*.log tv*.applog logs/$iter/

	if [[ "$(cat logs/$iter/tv*.log | grep fail | wc -l)" == "0" ]]
	then
		echo "all passed at iteration $iter"
		iter=$((iter+1))
		continue
	fi

	echo "at least one failed at iteration $iter"
	exit 1
done
